CREATE CONSTRAINT ON (xcommit:XCommit) ASSERT xcommit.sha IS UNIQUE;
CREATE (c1:XCommit {sha: 'b1', branch: 'bugfix'})
CREATE (c2:XCommit {sha: 'b2', branch: 'bugfix'})
CREATE (c3:XCommit {sha: 'b3', branch: 'bugfix'})
CREATE (c4:XCommit {sha: 'b4', branch: 'bugfix'})
CREATE (c5:XCommit {sha: 'b5', branch: 'bugfix'})
CREATE (c6:XCommit {sha: 'b6', branch: 'bugfix'})
CREATE (c7:XCommit {sha: 'b7', branch: 'bugfix'})
CREATE (c8:XCommit {sha: 'b8', branch: 'bugfix'})
CREATE (c9:XCommit {sha: 'b9', branch: 'bugfix'})
CREATE (c10:XCommit {sha: 'b10', branch: 'bugfix'})

CREATE (c1)-[:XNEXT]->(c2)
CREATE (c2)-[:XNEXT]->(c3)
CREATE (c3)-[:XNEXT]->(c4)
CREATE (c4)-[:XNEXT]->(c5)
CREATE (c5)-[:XNEXT]->(c6)
CREATE (c6)-[:XNEXT]->(c7)
CREATE (c7)-[:XNEXT]->(c8)
CREATE (c8)-[:XNEXT]->(c9)
CREATE (c9)-[:XNEXT]->(c10)



CREATE (mc1:XCommit {sha: 'mb1', branch: 'master'})
CREATE (mc2:XCommit {sha: 'mb2', branch: 'master'})
CREATE (mc3:XCommit {sha: 'mb3', branch: 'master'})
CREATE (mc4:XCommit {sha: 'mb4', branch: 'master'})
CREATE (mc5:XCommit {sha: 'mb5', branch: 'master'})
CREATE (mc6:XCommit {sha: 'mb6', branch: 'master'})
CREATE (mc7:XCommit {sha: 'mb7', branch: 'master'})
CREATE (mc8:XCommit {sha: 'mb8', branch: 'master'})
CREATE (mc9:XCommit {sha: 'mb9', branch: 'master'})
CREATE (mc10:XCommit {sha: 'mb10', branch: 'master'})

CREATE (mc1)-[:XNEXT]->(mc2)
CREATE (mc2)-[:XNEXT]->(mc3)
CREATE (mc3)-[:XNEXT]->(mc4)
CREATE (mc4)-[:XNEXT]->(mc5)
CREATE (mc5)-[:XNEXT]->(mc6)
CREATE (mc6)-[:XNEXT]->(mc7)
CREATE (mc7)-[:XNEXT]->(mc8)
CREATE (mc8)-[:XNEXT]->(mc9)
CREATE (mc9)-[:XNEXT]->(mc10)

create (merge1:XMerge {title: 'Merge c3 nach mc4'})
create (merge2:XMerge {title: 'Merge c5 nach mc6'})
create (merge3:XMerge {title: 'Merge c8 nach mc9'})

create (c3)-[:DOWN_MERGE]->(mc4)
create (c5)-[:DOWN_MERGE]->(mc6)
create (c8)-[:DOWN_MERGE]->(mc9)

;

