package nh.cmsvis.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {

	public static void removeDir(String dir) {

		System.out.println("REMOVE " + dir);

		Path directoryToDelete = Paths.get(dir);
		DeletingFileVisitor delFileVisitor = new DeletingFileVisitor();
		try {
			Files.walkFileTree(directoryToDelete, delFileVisitor);
		} catch (IOException ex) {
			throw new IllegalStateException("Could not remove dir '" + dir
					+ "': " + ex, ex);
		}

	}

}
