package nh.cmsvis.oldimporter;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import nh.cmsvis.utils.FileUtils;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserters;

import de.svenjacobs.loremipsum.LoremIpsum;

public class DatabaseImporter {

	private final static String DB_DIR = "/Users/nils/develop/neo4j/neo4j-community-2.0.3/data/graph.db";

	private final BatchInserter _inserter;
	private final RelationshipType parentRelShip = DynamicRelationshipType
			.withName("PARENT");
	private final RelationshipType commitRelShip = DynamicRelationshipType
			.withName("COMMIT");
	private final RelationshipType firstParentRelShip = DynamicRelationshipType
			.withName("FIRST_PARENT");
	private final RelationshipType implementedInRelShip = DynamicRelationshipType
			.withName("IMPLEMENTED_IN");

	private final RelationshipType mergedByRelShip = DynamicRelationshipType
			.withName("MERGED_BY");

	private final RelationshipType builtRelShip = DynamicRelationshipType
			.withName("BUILT");

	private final RelationshipType releasedRelShip = DynamicRelationshipType
			.withName("RELEASED");

	private final RelationshipType nachfolgerRelShip = DynamicRelationshipType
			.withName("NACHFOLGER");

	private final RelationshipType affectsRelShip = DynamicRelationshipType
			.withName("AFFECTS");

	private final RelationshipType prevReleaseRelShip = DynamicRelationshipType
			.withName("PREV_RELEASE");

	private Label _commitLabel;

	private Label _changeLabel;

	private Label _buildLabel;

	private Label _releaseLabel;

	private int commitCounter;

	private Label _eventLabel;

	private int eventCounter = 10000;

	private ReleaseCounter releaseCounter = new ReleaseCounter();

	private final TimeGenerator timeGenerator = new TimeGenerator();

	class TimeGenerator {
		private final Random r = new Random();
		private final long MINUTE = 60 * 1000;
		private final long FIVE_MINUTES = 5 * MINUTE;
		long current = System.currentTimeMillis();

		public long next() {
			current += (r.nextInt(60) + MINUTE) + FIVE_MINUTES;
			return current;
		}
	}

	interface CommitListener {
		void commitInserted(Commit c);
	}

	class ReleaseCounter {
		private int minor = 0;
		private int major = 14;
		private long releaseNodeId;

		public ReleaseCounter newIntRelease() {
			major++;
			minor = 1;
			return this;
		}

		public ReleaseCounter newBugfixRelease() {
			minor++;
			return this;
		}

		public String asString() {
			return major + ".0" + minor;
		}
	}

	static class Change {
		private final static Map<String, Integer> ids = new Hashtable<>();
		private final String changeId;
		private long nodeId;
		private Commit newestCommit;
		public int commitCount;

		Change(String keyPrefix) {
			if (!ids.containsKey(keyPrefix)) {
				ids.put(keyPrefix, 9999);
			}
			int number = ids.get(keyPrefix) + 1;
			ids.put(keyPrefix, number);

			this.changeId = keyPrefix + "-" + number;
		}
	}

	static class Commit {
		static int NUMBER = 100000;
		private final String sha = UUID.randomUUID().toString();
		private final int number = NUMBER++;
		private long nodeId;
		private String message;

		public Commit() {
			this.message = "Commit #" + number;
		}

		public Commit(String message) {
			this.message = message;
		}

		@Override
		public String toString() {
			return "Commit [sha=" + sha + ", number=" + number + ", nodeId="
					+ nodeId + "]";
		}

	}

	public DatabaseImporter() {
		this._inserter = BatchInserters.inserter(DB_DIR,
				new Hashtable<String, String>());
	}

	final LoremIpsum loremIpsum = new LoremIpsum();

	private Change createChange(final Commit parent, String branch, int commits) {
		String jiraProject = "";
		if ("bugfix".equals(branch)) {
			jiraProject = "TABUG";
		} else {
			jiraProject = "TAINT";
		}
		final Change change = new Change(jiraProject);
		change.commitCount = commits;
		Map<String, Object> properties = createDefaultProperties(change.changeId);
		properties.put("changeId", change.changeId);
		properties.put("commitCount", change.commitCount);

		String words = loremIpsum.getWords(random(10) + 1, random(49) + 1);
		words = Character.toUpperCase(words.charAt(0)) + words.substring(1);

		properties.put(OBJECT_ID, words);

		change.nodeId = _inserter.createNode(properties, _changeLabel);

		final Commit lastCommit = insertCommits(parent, commits,
				new CommitListener() {

					@Override
					public void commitInserted(Commit c) {
						_inserter.createRelationship(change.nodeId, c.nodeId,
								implementedInRelShip, null);
					}
				});

		change.newestCommit = lastCommit;

		return change;

	}

	Random r = new Random();

	private int random(int m) {
		return r.nextInt(m);
	}

	StringBuilder prot = new StringBuilder();

	private final static int CHANGES = 20000;
	private final static int MIN_COMMITS_PER_CHANGE = 2;
	private final static int MAX_COMMITS_PER_CHANGE = 35;

	private final List<Commit> bugfixFirstParentCommits = new LinkedList<>();
	private final List<Commit> intFirstParentCommits = new LinkedList<>();

	private final List<Change> changes = new LinkedList<>();

	public void importDb() {
		setupLabels();

		System.out.println("Import Root Commit");
		final Commit initialBugfixCommit = insertCommit(new Commit(
				"Initial Bugfix Commit"));

		bugfixFirstParentCommits.add(initialBugfixCommit);
		intFirstParentCommits.add(initialBugfixCommit);

		int pullRequests = 1000;
		Commit currentBugfixCommit = initialBugfixCommit;
		Commit currentIntCommit = initialBugfixCommit;

		int loop = 0;

		boolean lastBugfixToIntSuccess = false;

		while (changes.size() < CHANGES + 1) {
			loop++;
			final int action = random(5);

			if ((loop) % 3 == 0) {
				// nur build
				createBuild(currentBugfixCommit, "bugfix");
				createBuild(currentIntCommit, "int");
			}

			if ((loop) % 100 == 0) {
				// neues Major-Release
				createRelease(currentIntCommit, "int");
			}

			if ((loop) % 10 == 0) {
				// jedes zehnte Mal Bugfix-Release
				createRelease(currentBugfixCommit, "bugfix");
			}

			if (action < 2) {
				createEvent(currentBugfixCommit, "DT01");
				createEvent(currentBugfixCommit, "DT02");
			}
			if (action > 3) {
				lastBugfixToIntSuccess = !lastBugfixToIntSuccess;

				if (lastBugfixToIntSuccess) {
					// Erfolgreicher Merge => Commit
					currentIntCommit = insertCommit(new Commit(
							"Merge bugfix nach int"), currentIntCommit,
							bugfixFirstParentCommits
									.get(bugfixFirstParentCommits.size() - 1));
					createEvent(currentIntCommit, "merge_bugfix_int", "ok");

				} else {
					createEvent(currentBugfixCommit, "merge_bugfix_int",
							"problem");
				}
			}

			// / NEUER CHANGE
			boolean bugfixChange = random(10) < 3;
			List<Commit> firstParents = bugfixChange ? bugfixFirstParentCommits
					: intFirstParentCommits;

			int pc = 0;
			if (firstParents.size() > 3) {
				pc = firstParents.size() - (random(2) + 1);
			} else {
				pc = firstParents.size() - 1;
			}

			final Commit start = firstParents.get(pc);

			Change change = createChange(start,
					bugfixChange ? "bugfix" : "int",
					random(MAX_COMMITS_PER_CHANGE) + MIN_COMMITS_PER_CHANGE);
			changes.add(change);

			int pr = ++pullRequests;
			Commit pullRequestCommit = insertCommit(new Commit("PR" + pr + ": "
					+ change.changeId), start, change.newestCommit);
			_inserter.createRelationship(change.nodeId,
					pullRequestCommit.nodeId, mergedByRelShip, null);
			createEvent(pullRequestCommit, "pullrequest_merged", "ok",
					("PR" + pr));

			firstParents.add(pullRequestCommit);
			_inserter.createRelationship(pullRequestCommit.nodeId,
					start.nodeId, firstParentRelShip, null);
			_inserter.createRelationship(start.nodeId,
					pullRequestCommit.nodeId, nachfolgerRelShip, null);

			prot.append(String
					.format("Pullrequest: #%d Change: %s Branch: %s, Commits fuer Change: %d%n",
							pr, change.changeId, (bugfixChange ? "bugfix"
									: "int"), change.commitCount));
			prot.append(String.format("  First Parent: %s -> %s%n",
					pullRequestCommit.message, start.message));

			// if (action < 4) {
			// // außerdem build ausführen
			// createBuild(pullRequestCommit);
			// }
			if (bugfixChange) {
				currentBugfixCommit = pullRequestCommit;
			} else {
				currentIntCommit = pullRequestCommit;
			}

		}

		System.out.println("Shutdown...");
		_inserter.shutdown();

		System.out.println(prot);

	}

	private final Map<String, Integer> buildCounter = new Hashtable<>();

	private void createBuild(Commit start, String branch) {

		if (!buildCounter.containsKey(branch)) {
			buildCounter.put(branch, 999);
		}

		int counter = buildCounter.get(branch);
		counter++;

		String buildId = branch + "_" + "build_" + counter;
		buildCounter.put(branch, counter);

		Map<String, Object> properties = createDefaultProperties(buildId);
		properties.put("buildId", buildId);
		properties.put("time", timeGenerator.next());

		prot.append(String.format("Build %s fuer Commit %s (%s)%n", buildId,
				start.number, start.message));

		long buildNodeId = _inserter.createNode(properties, _buildLabel);

		_inserter.createRelationship(buildNodeId, start.nodeId, builtRelShip,
				null);
		_inserter.createRelationship(buildNodeId, start.nodeId, commitRelShip,
				null);

	}

	private void createRelease(Commit start, String branch) {
		String version;
		if ("bugfix".equals(branch)) {
			version = "R_" + branch + "_"
					+ releaseCounter.newBugfixRelease().asString();
		} else {
			version = "R_" + branch + "_"
					+ releaseCounter.newIntRelease().asString();
		}

		Map<String, Object> properties = createDefaultProperties(version);
		properties.put("version", version);

		prot.append(String.format("%s-Release %s fuer Commit %s (%s)%n",
				branch, version, start.number, start.message));

		long relNodeId = _inserter.createNode(properties, _releaseLabel);

		_inserter.createRelationship(relNodeId, start.nodeId, releasedRelShip,
				null);
		_inserter.createRelationship(relNodeId, start.nodeId, commitRelShip,
				null);

		if (releaseCounter.releaseNodeId > 0) {
			_inserter.createRelationship(relNodeId,
					releaseCounter.releaseNodeId, prevReleaseRelShip, null);
		}

		releaseCounter.releaseNodeId = relNodeId;
	}

	private final static String OBJECT_ID = "objectId";
	private final static String LINK = "link";
	private final static String TIME = "time";
	private final static String STATE = "state"; // optional

	private Map<String, Object> createDefaultProperties(String objectId) {
		Map<String, Object> properties = new HashMap<>();

		properties.put(OBJECT_ID, objectId);
		properties.put(LINK, "http://cms-server/" + OBJECT_ID);
		properties.put(TIME, timeGenerator.next());

		return properties;
	}

	private void createEvent(Commit start, String type, String state) {
		String eventId = "E_" + (eventCounter++);

		createEvent(start, type, state, eventId);
	}

	private void createEvent(Commit start, String type, String state,
			String objectId) {

		Map<String, Object> properties = createDefaultProperties(objectId);
		properties.put("cause", type);
		properties.put(STATE, state);

		prot.append(String.format("Event %s (%s) fuer Commit %s (%s)%n",
				objectId, type, start.number, start.message));

		long buildNodeId = _inserter.createNode(properties, _eventLabel);

		_inserter.createRelationship(buildNodeId, start.nodeId, affectsRelShip,
				null);
	}

	private void createEvent(Commit start, String type) {
		createEvent(start, type, "ok");

	}

	private Commit insertCommits(final Commit root, final int count,
			final CommitListener cl) {

		int counter = 0;
		Commit parent = root;
		for (counter = 0; counter < count; counter++) {

			Commit insertedCommit = insertCommit(new Commit(), parent);
			if (cl != null) {
				cl.commitInserted(insertedCommit);
			}

			parent = insertedCommit;

		}

		// parent ist der zuletzt eingefuegte commit
		return parent;

	}

	private Commit insertCommit(Commit commit, Commit... parents) {
		if (commit.nodeId != 0) {
			throw new RuntimeException("Commit " + commit
					+ " existiert bereits in der DB!");
		}
		Map<String, Object> properties = createDefaultProperties(commit.sha);

		properties.put("sha", commit.sha);
		properties.put("number", "c" + commit.number);
		properties.put("message", commit.message);
		if (parents != null && parents.length > 0) {
			List<String> parentIds = new java.util.LinkedList<>();
			for (Commit parent : parents) {
				parentIds.add("c" + parent.number);

			}
			properties.put("parents", parentIds.toArray(new String[0]));
		}

		// System.out.println("  Import Commit " + commit.number);
		commit.nodeId = _inserter.createNode(properties, _commitLabel);
		if (parents != null) {
			for (Commit parent : parents) {
				_inserter.createRelationship(commit.nodeId, parent.nodeId,
						parentRelShip, null);
			}
		}

		return commit;

	}

	private void setupLabels() {
		_commitLabel = newLabel("Commit", "sha");
		_changeLabel = newLabel("Change", "changeId");
		_buildLabel = newLabel("Build", "buildId");
		_releaseLabel = newLabel("Release", "version");
		_eventLabel = newLabel("Event", "eventId");
		// _inserter.createDeferredConstraint(_eventLabel)
		// .assertPropertyIsUnique("cause").create();
		_inserter.createDeferredSchemaIndex(_eventLabel).on("cause").create();
		// Constraints
	}

	public Label newLabel(String title, String uniqueKey) {

		Label label = DynamicLabel.label(title);
		_inserter.createDeferredConstraint(label)
				.assertPropertyIsUnique(uniqueKey).create();
		// _inserter.createDeferredConstraint(label)
		// .assertPropertyIsUnique(OBJECT_ID).create();

		return label;
	}

	public static void main(String[] args) {

		FileUtils.removeDir(DB_DIR);

		DatabaseImporter importer = new DatabaseImporter();
		importer.importDb();

	}
}
