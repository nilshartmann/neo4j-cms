package nh.cmsvis.importer;

import nh.cmsvis.importer.model.Branch;
import nh.cmsvis.importer.model.Branches;
import nh.cmsvis.importer.model.Commit;

public class Szenario {

	private static Szenario INSTANCE;

	public final static Szenario get() {
		return INSTANCE;
	}

	private final Branch _masterBranch;
	private final Branch _integrationBranch;
	private final Branch _bugfixBranch;
	private final Branch _prodBranch;

	private final Branch[] _developmentBranches;

	public static void setupSzenario() {
		INSTANCE = new Szenario();

	}

	private Szenario() {

		Commit rootCommit = new Commit("Root Commit");
		rootCommit.save();

		this._masterBranch = new Branch(Branches.master.toString(), rootCommit,
				true);
		this._integrationBranch = new Branch(Branches.integration.toString(),
				rootCommit, true);
		this._bugfixBranch = new Branch(Branches.bugfix.toString(), rootCommit,
				true);
		this._prodBranch = new Branch(Branches.prod.toString(), rootCommit,
				true);

		_developmentBranches = new Branch[] { _masterBranch,
				_integrationBranch, _bugfixBranch };

	}

	public Branch[] getDevelopmentBranches() {
		return _developmentBranches;
	}

	public Branch getMasterBranch() {
		return _masterBranch;
	}

	public Branch getIntegrationBranch() {
		return _integrationBranch;
	}

	public Branch getBugfixBranch() {
		return _bugfixBranch;
	}

	public Branch getProdBranch() {
		return _prodBranch;
	}

	public Branch getBranch(Branches branch) {
		switch (branch) {
		case master:
			return _masterBranch;
		case integration:
			return _integrationBranch;
		case bugfix:
			return _bugfixBranch;
		case prod:
			return _prodBranch;
		default:
		}

		throw new IllegalStateException("Branch '" + branch + "' nicht bekannt");

	}

	public Branch getRandomBranch() {
		int n = Utils.get().randomInt(_developmentBranches.length);

		return _developmentBranches[n];
	}

}
