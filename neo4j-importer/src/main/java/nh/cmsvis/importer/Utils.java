package nh.cmsvis.importer;

import java.util.Hashtable;
import java.util.Map;
import java.util.Random;

import de.svenjacobs.loremipsum.LoremIpsum;

public class Utils {

	private final Random _random = new Random();

	private final static Utils INSTANCE = new Utils();

	private final Map<String, Integer> _counter = new Hashtable<>();
	private final LoremIpsum loremIpsum = new LoremIpsum();
	private final Time _time = new Time();

	public final static Utils get() {
		return INSTANCE;
	}

	public int randomInt(int toExclusive) {
		return _random.nextInt(toExclusive);
	}

	public int randomIntBetween(int fromInclusive, int toInclusive) {
		return _random.nextInt(toInclusive + 1) + fromInclusive;
	}

	public boolean randomBoolean() {
		return _random.nextInt(5) < 4;
	}

	public int nextCounter(String key) {
		if (!_counter.containsKey(key)) {
			_counter.put(key, 0);
		}

		int n = _counter.get(key);

		n++;
		_counter.put(key, n);
		return n;
	}

	public String randomWords(int minWords, int maxWords) {
		String words = loremIpsum.getWords(
				randomIntBetween(minWords, maxWords), randomIntBetween(0, 49));
		words = Character.toUpperCase(words.charAt(0)) + words.substring(1);

		return words;
	}

	public long nextTime() {
		return _time.next();
	}

	public long currentTime() {
		return _time.current();
	}
}
