package nh.cmsvis.importer;

import java.util.Hashtable;
import java.util.Map;

import nh.cmsvis.utils.FileUtils;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserters;

public class Database {

	private static Database INSTANCE;

	public static Database get() {
		return INSTANCE;
	}

	public static void create(String dbDir) {
		FileUtils.removeDir(dbDir);
		Database database = new Database(dbDir);
		INSTANCE = database;
	}

	private final BatchInserter _inserter;

	// GIT_xyz-Beziehungen spiegeln die "echten" Git-Beziehungen der Commits
	// wieder
	public final static RelationshipType GIT_parentRelShip = DynamicRelationshipType
			.withName("GIT_PARENT");
	public final static RelationshipType GIT_firstParentRelShip = DynamicRelationshipType
			.withName("GIT_FIRST_PARENT");

	/** "Semantischer" Vorgänger, nach dem Branch-Modell (nur für commits) */
	public final static RelationshipType prevCommitRelShip = DynamicRelationshipType
			.withName("PREV_COMMIT");

	public final static RelationshipType nextCommitRelShip = DynamicRelationshipType
			.withName("NEXT_COMMIT");

	public final static RelationshipType firstNextCommitRelShip = DynamicRelationshipType
			.withName("FIRST_NEXT_COMMIT");

	/**
	 * Beziehung von einem nicht-Commit auf ein Commit, z.B. event->commit oder
	 * build->commit
	 */
	public final static RelationshipType commitRelShip = DynamicRelationshipType
			.withName("COMMIT");
	public final static RelationshipType mergeDownRelShip = DynamicRelationshipType
			.withName("MERGE_DOWN");
	public final static RelationshipType implementedInRelShip = DynamicRelationshipType
			.withName("IMPLEMENTED_IN");

	public final static RelationshipType eventRelShip = DynamicRelationshipType
			.withName("EVENT");

	/**
	 * @deprecated {@link #commitRelShip} verwenden
	 */
	public final static RelationshipType mergedByRelShip = DynamicRelationshipType
			.withName("MERGED_BY");

	/**
	 * @deprecated {@link #commitRelShip} verwenden
	 */
	@Deprecated
	public final static RelationshipType builtRelShip = DynamicRelationshipType
			.withName("BUILT");

	public final static RelationshipType releasedRelShip = DynamicRelationshipType
			.withName("RELEASED");

	public final static RelationshipType nachfolgerRelShip = DynamicRelationshipType
			.withName("NACHFOLGER");

	public final static RelationshipType prevReleaseRelShip = DynamicRelationshipType
			.withName("PREV_RELEASE");

	public Label commitLabel;

	public Label changeLabel;

	public Label buildLabel;

	public Label releaseLabel;

	public Label eventLabel;

	public Label headLabel;

	public Label mergeLabel;

	public Database(String dbDir) {
		this._inserter = BatchInserters.inserter(dbDir,
				new Hashtable<String, String>());

		setupLabels();

		setupIndices();
	}

	private void setupLabels() {
		commitLabel = newLabel("Commit", "sha");
		changeLabel = newLabel("Change", "changeId");
		buildLabel = newLabel("Build", "buildId");
		releaseLabel = newLabel("Release", "version");
		eventLabel = newLabel("Event", "eventId");
		headLabel = newLabel("Head", "branch");
		mergeLabel = newLabel("Merge", "merge");
	}

	private void setupIndices() {

		// Index
		_inserter.createDeferredSchemaIndex(eventLabel).on("cause").create();
		_inserter.createDeferredSchemaIndex(commitLabel).on("toBranch")
				.create();
	}

	public Label newLabel(String title, String uniqueKey) {

		Label label = DynamicLabel.label(title);
		_inserter.createDeferredConstraint(label)
				.assertPropertyIsUnique(uniqueKey).create();
		_inserter.createDeferredConstraint(label)
				.assertPropertyIsUnique("objectId").create();

		// _inserter.createDeferredSchemaIndex(label).on("objectId").create();

		return label;
	}

	public static void shutdown() {
		System.out.println("Shutdown Database");
		INSTANCE._inserter.shutdown();
		INSTANCE = null;
	}

	public long createNode(Map<String, Object> properties, Label label) {
		return _inserter.createNode(properties, label);
	}

	public void createRelShip(long from, long to, RelationshipType type) {
		_inserter.createRelationship(from, to, type, null);
	}

	public Label getHeadLabel() {
		return this.headLabel;
	}

	public Label getMergeLabel() {
		return mergeLabel;
	}

}
