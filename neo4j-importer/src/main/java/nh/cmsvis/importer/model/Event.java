package nh.cmsvis.importer.model;

import java.util.Map;

import nh.cmsvis.importer.Database;

public class Event {

	private final EventState _eventState;
	private final String _cause;
	private final String _objectId;
	private final String _description;
	private long _nodeId;

	public Event(EventState eventState, String cause, String objectId,
			String description) {
		super();
		this._eventState = eventState;
		this._objectId = objectId;
		this._cause = cause;
		this._description = description
				+ (eventState == EventState.problem ? " (fehlgeschlagen)" : "");
	}

	public Event(EventState eventState, EventCause cause, String objectId,
			String description) {
		this(eventState, cause.name(), objectId, description);
	}

	public static Event save(String objectId, String cause, Commit target,
			EventState state, String description) {
		Event event = new Event(state, cause, objectId, description);
		return event.save(target);
	}

	public static Event save(String objectId, EventCause cause, Commit target,
			EventState state, String description) {
		Event event = new Event(state, cause, objectId, description);
		return event.save(target);
	}

	public static Event saveOk(String objectId, EventCause cause,
			Commit target, String description) {
		return save(objectId, cause, target, EventState.ok, description);
	}

	public Event save(Commit target) {

		Map<String, Object> properties = NodeProperties
				.createDefaultProperties(_objectId, _description);
		properties.put(NodeProperties.state.name(), _eventState.name());
		properties.put("cause", _cause);
		properties.put("commit-number", target.getNumber());

		Database database = Database.get();
		this._nodeId = database.createNode(properties, database.eventLabel);

		// Event-[:COMMIT]->Commit
		database.createRelShip(_nodeId, target.getNodeId(),
				Database.commitRelShip);
		// Commit-[:EVENT]->Event
		database.createRelShip(target.getNodeId(), _nodeId,
				Database.eventRelShip);

		return this;

	}
}
