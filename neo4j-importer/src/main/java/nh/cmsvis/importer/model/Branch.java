package nh.cmsvis.importer.model;

public class Branch {

	private final Commit _start;
	private Commit _head;
	private final String _name;

	private final boolean _trackParentHistory;

	public Branch(String name, Branch start) {
		this(name, start._head, false);
	}

	public Branch(String name, Commit start, boolean trackParentHistory) {
		this._name = name;
		this._start = start;
		this._trackParentHistory = trackParentHistory;
		adjustHead(start);
	}

	public String getName() {
		return _name;
	}

	public Commit commit(String message) {
		Commit commit = new Commit(message, this._head);
		commit.setFirstParent(this._head);
		if (_trackParentHistory) {
			commit.addPreviousCommit(this._head);
		}
		commit.save();

		adjustHead(commit);
		return commit;
	}

	public Commit merge(Branch otherBranch) {
		Commit mergeCommit = new Commit("Merge " + otherBranch.getName()
				+ " into " + this._name, _head, otherBranch.getHead());
		mergeCommit.setFirstParent(_head);
		if (_trackParentHistory) {
			mergeCommit.addPreviousCommit(this._head);

			if (otherBranch.isTrackParentHistory()) {
				mergeCommit.addPreviousCommit(otherBranch.getHead());
			}
		}

		mergeCommit.addProperty("fromBranch", otherBranch.getName());
		mergeCommit.addProperty("toBranch", _name);

		mergeCommit.save();

		adjustHead(mergeCommit);

		return mergeCommit;

	}

	private void adjustHead(Commit newHead) {
		_head = newHead;
	}

	public Commit getHead() {
		return _head;
	}

	public boolean isTrackParentHistory() {
		return _trackParentHistory;
	}
}
