package nh.cmsvis.importer.model;

import java.util.Map;

import nh.cmsvis.importer.Database;

public class Change {

	private final String _changeId;
	private final String _summary;
	private final Branch _targetBranch;
	private final Branch _topicBranch;

	private long _nodeId = -1;

	public Change(String changeId, String summary, Branch targetBranch,
			Branch topicBranch) {
		this._changeId = changeId;
		this._summary = summary;
		this._targetBranch = targetBranch;
		this._topicBranch = topicBranch;
	}

	public String getChangeId() {
		return _changeId;
	}

	public Branch getTargetBranch() {
		return _targetBranch;
	}

	public Branch getTopicBranch() {
		return _topicBranch;
	}

	public String getSummary() {
		return _summary;
	}

	public Change save() {
		Map<String, Object> properties = NodeProperties
				.createDefaultProperties(_changeId, _summary);
		properties.put("changeId", _changeId);
		properties.put("targetBranch", _targetBranch.getName());
		properties.put("topicBranch", _topicBranch.getName());
		properties.put("summary", _summary);

		this._nodeId = Database.get().createNode(properties,
				Database.get().changeLabel);

		return this;
	}

	public long getNodeId() {
		if (_nodeId < 0) {
			throw new IllegalStateException();
		}

		return _nodeId;
	}
}
