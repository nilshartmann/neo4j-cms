package nh.cmsvis.importer.model;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import nh.cmsvis.importer.Database;

public class Commit {
	static int NUMBER = 1;
	private final String sha = UUID.randomUUID().toString();
	private final int number = NUMBER++;
	private long nodeId = -1L;
	private String message;
	/** GIT Parent */
	private List<Commit> parents = null;

	/** GIT First Parent */
	private Commit _firstParent;

	private Map<String, Object> _properties;

	/**
	 * "Vereinfachter", "semantischer" Vorgänger:
	 * <ul>
	 * <li>first-parent auf einem Arbeitsbranch
	 * <li>beide Parents bei einem Merge Arbeitsbranch-Arbeitsbranch
	 * </ul>
	 */
	private List<Commit> _previousCommits = null;

	public Commit() {
		this.message = "Commit #" + number;
	}

	public Commit(String message, Commit... parents) {
		this.message = message;

		for (Commit parent : parents) {
			addParent(parent);
		}

	}

	public Commit(String message) {
		this.message = message;
	}

	public long getNodeId() {
		if (nodeId < 0) {
			throw new IllegalStateException("Commit #" + number + " ("
					+ message + ") noch nicht gesichert!");

		}
		return nodeId;
	}

	public boolean hasParents() {
		return this.parents != null;
	}

	public void addParent(Commit parent) {
		if (parents == null) {
			parents = new LinkedList<>();
		}

		parents.add(parent);
	}

	public Commit save() {
		Map<String, Object> properties = NodeProperties
				.createDefaultProperties(sha, message);

		properties.put("sha", sha);
		properties.put("number", number);
		properties.put("message", message);
		if (hasParents()) {
			List<String> parentIds = new java.util.LinkedList<>();
			for (Commit parent : parents) {
				parentIds.add("c" + parent.number);

			}
			properties.put("parents", parentIds.toArray(new String[0]));
		}

		if (_firstParent != null) {
			properties.put("firstParentSha", _firstParent.getSha());
			properties.put("firstParentNumber", _firstParent.getNumber());
		}

		if (_previousCommits != null) {

			for (int i = 0; i < _previousCommits.size(); i++) {
				Commit previousCommit = _previousCommits.get(i);

				properties.put("prevSha" + i, previousCommit.getSha());
				properties.put("prevNumber" + i, previousCommit.getNumber());
			}

		}

		if (_properties != null) {
			properties.putAll(_properties);
		}

		// System.out.println("  Import Commit " + commit.number);

		Database database = Database.get();

		this.nodeId = database.createNode(properties,
				Database.get().commitLabel);
		if (hasParents()) {
			for (Commit parent : parents) {
				database.createRelShip(this.nodeId, parent.nodeId,
						Database.GIT_parentRelShip);
			}
		}

		if (_firstParent != null) {
			database.createRelShip(this.nodeId, _firstParent.getNodeId(),
					Database.GIT_firstParentRelShip);
		}

		if (_previousCommits != null) {

			Commit firstPrevCommit = null;

			for (Commit prevCommit : _previousCommits) {
				if (firstPrevCommit == null) {
					firstPrevCommit = prevCommit;
				}
				database.createRelShip(this.nodeId, prevCommit.getNodeId(),
						Database.prevCommitRelShip);
				database.createRelShip(prevCommit.getNodeId(), this.nodeId,
						Database.nextCommitRelShip);
			}
			// "First-Child"-Beziehung (Nachfolger ausschliesslich auf demselben
			// Branch)
			database.createRelShip(firstPrevCommit.getNodeId(), this.nodeId,
					Database.firstNextCommitRelShip);

		}

		return this;
	}

	public int getNumber() {
		return number;
	}

	@Override
	public String toString() {
		return "Commit [sha=" + sha + ", number=" + number + ", nodeId="
				+ nodeId + "]";
	}

	public String getMessage() {
		return this.message;
	}

	public String getSha() {
		return sha;
	}

	public void setFirstParent(Commit firstParent) {
		_firstParent = firstParent;
	}

	public void addPreviousCommit(Commit previousCommit) {
		if (this._previousCommits == null) {
			this._previousCommits = new LinkedList<>();
		}
		this._previousCommits.add(previousCommit);
	}

	public void addProperty(String key, String value) {
		if (_properties == null) {
			_properties = new Hashtable<String, Object>();
		}

		_properties.put(key, value);

	}

}