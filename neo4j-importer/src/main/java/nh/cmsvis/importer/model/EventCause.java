package nh.cmsvis.importer.model;

public enum EventCause {

	pullrequest_merged, DT01, DT02, DT03, release

}
