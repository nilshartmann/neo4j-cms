package nh.cmsvis.importer.model;

import java.util.Map;

import nh.cmsvis.importer.Database;
import nh.cmsvis.importer.Utils;

public class Build {

	private final String _buildId;
	private final Commit _commit;
	private final EventState _state;
	private final Branches _branch;

	private long _nodeId;

	public Build(Commit commit, Branches branch, EventState state) {
		String key = "BUILD_" + branch.name();
		_buildId = key + "_" + Utils.get().nextCounter(key);
		_branch = branch;
		_state = state;
		_commit = commit;
	}

	public Build save() {

		Map<String, Object> properties = NodeProperties
				.createDefaultProperties(_buildId,
						"Build auf " + _branch.name());
		properties.put(NodeProperties.state.name(), _state.name());

		properties.put("commit-number", _commit.getNumber());
		properties.put("branch", _branch.name());

		System.out.println("BUILD " + _buildId + " -> " + _commit.getMessage()
				+ " (C" + _commit.getNumber() + ")");

		Database database = Database.get();
		this._nodeId = database.createNode(properties, database.buildLabel);
		database.createRelShip(_nodeId, _commit.getNodeId(),
				Database.builtRelShip);

		// Build-[:COMMIT]->Commit
		database.createRelShip(_nodeId, _commit.getNodeId(),
				Database.commitRelShip);

		return this;

	}

	public String getObjectId() {
		return this._buildId;
	}
}
