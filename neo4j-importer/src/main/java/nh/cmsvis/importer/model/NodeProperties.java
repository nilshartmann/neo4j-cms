package nh.cmsvis.importer.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import nh.cmsvis.importer.Utils;

public enum NodeProperties {

	objectId, summary, link, time, timeStr, state;

	private final static Set<String> OBJECT_IDS = new HashSet<>();
	private final static SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat(
			"dd.MM.yyyy HH:mm:ss.S");

	public static Map<String, Object> createDefaultProperties(String objectId,
			String summary) {

		if (OBJECT_IDS.contains(objectId)) {
			throw new IllegalStateException("Duplicate objectid " + objectId);
		}

		OBJECT_IDS.add(objectId);

		Map<String, Object> properties = new HashMap<>();

		properties.put(NodeProperties.objectId.name(), objectId);
		properties.put(NodeProperties.summary.name(), summary);
		properties.put(link.name(), "http://cms-server/" + objectId);

		long creationTime = Utils.get().nextTime();
		properties.put(time.name(), creationTime);

		properties.put(timeStr.name(),
				TIME_FORMATTER.format(new Date(creationTime)));

		return properties;
	}

}
