package nh.cmsvis.importer.model;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import nh.cmsvis.importer.Utils;

public class Changes {

	private final static Changes INSTANCE = new Changes();

	public final static Changes get() {
		return INSTANCE;
	}

	private final List<Change> _unmergedChanges = new ArrayList<>(1000);

	private final Map<Branches, List<Change>> _mergedChangesPerBranch = new Hashtable<>();

	public void addUnmergedChange(Change change) {
		_unmergedChanges.add(change);
	}

	public boolean hasUnmergedChanges() {
		return !_unmergedChanges.isEmpty();
	}

	public Change getRandomUnmergedChange() {
		if (!hasUnmergedChanges()) {
			return null;
		}
		int r = Utils.get().randomInt(_unmergedChanges.size());

		return _unmergedChanges.get(r);
	}

	protected List<Change> getMergedChanges(Branches branch) {
		List<Change> changes = _mergedChangesPerBranch.get(branch);
		if (changes == null) {
			changes = new ArrayList<>(1000);
			_mergedChangesPerBranch.put(branch, changes);
		}

		return changes;
	}

	public void changeMerged(Change unmergedChange) {
		Branches branch = Branches.valueOf(unmergedChange.getTargetBranch()
				.getName());

		List<Change> changes = getMergedChanges(branch);

		_unmergedChanges.remove(unmergedChange);
		changes.add(unmergedChange);

	}

	public Change getNewestMergedChange(Branches branch) {
		List<Change> mergedChanges = getMergedChanges(branch);
		if (mergedChanges.isEmpty()) {
			return null;
		}

		return mergedChanges.get(mergedChanges.size() - 1);

	}
}
