package nh.cmsvis.importer.actors;

import nh.cmsvis.importer.Actor;
import nh.cmsvis.importer.ActorState;
import nh.cmsvis.importer.AllActors;
import nh.cmsvis.importer.Szenario;
import nh.cmsvis.importer.Utils;
import nh.cmsvis.importer.model.Branch;
import nh.cmsvis.importer.model.Change;

public class NewChangeActor implements Actor {

	private final int _maxChanges;

	private int changeCount = 0;

	public NewChangeActor(int maxChanges) {
		this._maxChanges = maxChanges;
	}

	@Override
	public ActorState act() throws Exception {

		if (Utils.get().randomIntBetween(1, 10) % 2 == 0) {
			newChange();
			changeCount++;
		}

		return (changeCount > _maxChanges ? ActorState.stopAll : ActorState.run);

	}

	protected void newChange() {
		int maxCommits = Utils.get().randomIntBetween(1, 25);

		Branch targetBranch = Szenario.get().getRandomBranch();
		String changePrefix = "TA" + targetBranch.getName().toUpperCase();
		String changeId = changePrefix + "-"
				+ Utils.get().nextCounter(changePrefix);
		Branch topicBranch = new Branch(changeId, targetBranch);
		String summary = Utils.get().randomWords(3, 10);

		System.out.println("NEW CHANGE: " + changeId);

		Change change = new Change(changeId, summary, targetBranch, topicBranch);
		change.save();

		ChangeActor changeActor = new ChangeActor(change, maxCommits);
		AllActors.get().addActor(changeActor);

	}

}
