package nh.cmsvis.importer.actors;

import nh.cmsvis.importer.Actor;
import nh.cmsvis.importer.ActorState;
import nh.cmsvis.importer.Database;
import nh.cmsvis.importer.Utils;
import nh.cmsvis.importer.model.Change;
import nh.cmsvis.importer.model.Changes;
import nh.cmsvis.importer.model.Commit;
import nh.cmsvis.importer.model.Event;
import nh.cmsvis.importer.model.EventCause;

public class PullrequestActor implements Actor {

	@Override
	public ActorState act() throws Exception {
		Changes changes = Changes.get();

		if (changes.hasUnmergedChanges()) {
			Change unmergedChange = changes.getRandomUnmergedChange();

			System.out.println("PULL REQUEST FOR "
					+ unmergedChange.getChangeId());

			Commit pullrequestCommit = unmergedChange.getTargetBranch().merge(
					unmergedChange.getTopicBranch());

			Event.saveOk("PR" + Utils.get().nextCounter("PR"),
					EventCause.pullrequest_merged, pullrequestCommit,
					"Merge Pullrequest for " + unmergedChange.getChangeId());

			Database.get().createRelShip(unmergedChange.getNodeId(),
					pullrequestCommit.getNodeId(), Database.mergedByRelShip);

			// Change-[:COMMIT]->Commit
			Database.get().createRelShip(unmergedChange.getNodeId(),
					pullrequestCommit.getNodeId(), Database.commitRelShip);

			changes.changeMerged(unmergedChange);
		}

		return ActorState.run;

	}
}
