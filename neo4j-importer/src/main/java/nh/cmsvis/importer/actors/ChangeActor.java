package nh.cmsvis.importer.actors;

import nh.cmsvis.importer.Actor;
import nh.cmsvis.importer.ActorState;
import nh.cmsvis.importer.Utils;
import nh.cmsvis.importer.model.Change;
import nh.cmsvis.importer.model.Changes;

public class ChangeActor implements Actor {
	private Change _change;
	private final int _maxCommits;

	private int _commitCount;

	ChangeActor(Change change, int maxCommits) {
		this._change = change;
		this._maxCommits = maxCommits;
	}

	@Override
	public ActorState act() throws Exception {

		if (_commitCount >= _maxCommits) {
			// Fertig
			System.out.println("CHANGE " + _change.getChangeId() + " FINISHED");
			Changes.get().addUnmergedChange(_change);
			return ActorState.finished;
		}

		if (_commitCount < _maxCommits) {
			_commitCount++;

			if (_commitCount % 3 == 0) {
				// Update from ...
				// System.out.println("UPDATE FROM "
				// + _change.getTargetBranch().getName() + " FOR CHANGE "
				// + _change.getChangeId());
				// _change.getTopicBranch().merge(_change.getTargetBranch());
			} else {

				System.out.println("NEW COMMIT FOR CHANGE "
						+ _change.getChangeId());
				_change.getTopicBranch().commit(
						_change.getChangeId() + ": "
								+ Utils.get().randomWords(2, 5));
			}
		}

		// weitermachen
		return ActorState.run;
	}
}
