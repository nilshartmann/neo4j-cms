package nh.cmsvis.importer.actors;

import java.util.Map;

import nh.cmsvis.importer.Database;
import nh.cmsvis.importer.Szenario;
import nh.cmsvis.importer.Utils;
import nh.cmsvis.importer.model.Branch;
import nh.cmsvis.importer.model.Branches;
import nh.cmsvis.importer.model.Commit;
import nh.cmsvis.importer.model.Event;
import nh.cmsvis.importer.model.EventState;
import nh.cmsvis.importer.model.NodeProperties;

public class MergeActor extends BranchRelatedActor {

	private final String _cause;
	private final Branches _to;

	private final Branch _fromBranch;
	private final Branch _toBranch;

	public MergeActor(Branches from, Branches to) {
		super(from);
		_to = to;
		_cause = "merge_" + from.name() + "_" + to.name();

		_fromBranch = Szenario.get().getBranch(getBranch());
		_toBranch = Szenario.get().getBranch(_to);

	}

	@Override
	protected Commit doAct(final Commit newCommit) {

		System.out.println("MERGE " + getBranch() + "(C"
				+ newCommit.getNumber() + ") to " + _toBranch.getName() + " (C"
				+ _toBranch.getHead().getNumber() + ")");

		Commit mergeCommit = _toBranch.merge(_fromBranch);

		Event.save("ME_" + Utils.get().nextCounter("AUTOMERGE"), _cause,
				mergeCommit, EventState.ok,
				"Merged in C#" + mergeCommit.getNumber());

		// Merge-Knoten
		Map<String, Object> properties = NodeProperties
				.createDefaultProperties(
						"M_" + Utils.get().nextCounter("MERGES"), "Auto-Merge "
								+ getBranch() + "(C" + newCommit.getNumber()
								+ ") to " + _toBranch.getName() + " (C"
								+ _toBranch.getHead().getNumber() + ")");
		final String fromTo = _fromBranch.getName() + "_" + _toBranch.getName();
		properties.put("merge", fromTo + "_" + Utils.get().nextCounter(fromTo));
		Database database = Database.get();
		final long mergeNodeId = database.createNode(properties,
				database.getMergeLabel());
		// (merge)-[:COMMIT]->(mergeCommit)
		database.createRelShip(mergeNodeId, mergeCommit.getNodeId(),
				Database.commitRelShip);

		database.createRelShip(_fromBranch.getHead().getNodeId(),
				mergeCommit.getNodeId(), Database.mergeDownRelShip);

		return newCommit;
	}
}
