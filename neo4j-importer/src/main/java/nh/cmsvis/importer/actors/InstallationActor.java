package nh.cmsvis.importer.actors;

import java.text.SimpleDateFormat;
import java.util.Date;

import nh.cmsvis.importer.Utils;
import nh.cmsvis.importer.model.Branches;
import nh.cmsvis.importer.model.Commit;
import nh.cmsvis.importer.model.Event;
import nh.cmsvis.importer.model.EventCause;
import nh.cmsvis.importer.model.EventState;

public class InstallationActor extends BranchRelatedActor {

	private final EventCause _cause;
	int _count;

	public InstallationActor(Branches branch, EventCause cause) {
		super(branch);
		this._cause = cause;
	}

	@Override
	protected Commit doAct(final Commit newCommit) {
		if (Utils.get().randomIntBetween(1, 9) % 3 == 0) {
			_count++;
			Event.save("I_" + Utils.get().nextCounter("INSTALL"), _cause,
					newCommit, Utils.get().randomBoolean() ? EventState.ok
							: EventState.problem, _count + ". Installation am "
							+ currentDate());
		}
		return newCommit;
	}

	private String currentDate() {
		SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		return f.format(new Date(Utils.get().currentTime()));
	}

}
