package nh.cmsvis.importer.actors;

import java.util.Map;

import nh.cmsvis.importer.Actor;
import nh.cmsvis.importer.ActorState;
import nh.cmsvis.importer.Database;
import nh.cmsvis.importer.model.Branch;
import nh.cmsvis.importer.model.NodeProperties;

public class SetHeadActor implements Actor {

	private Branch[] _branches;

	public SetHeadActor(Branch[] branches) {
		this._branches = branches;
	}

	@Override
	public ActorState act() throws Exception {

		Database database = Database.get();

		for (Branch branch : _branches) {

			String objectId = "HEAD_" + branch.getName().toUpperCase();

			System.out.println("HEAD: " + objectId);

			Map<String, Object> properties = NodeProperties
					.createDefaultProperties(objectId,
							"HEAD " + branch.getName() + " (C"
									+ branch.getHead().getNumber() + ")");
			properties.put("branch", branch.getName());

			final long nodeId = database.createNode(properties,
					database.getHeadLabel());

			// (head)-[:COMMIT]->(commit)
			database.createRelShip(nodeId, branch.getHead().getNodeId(),
					Database.commitRelShip);

		}

		return null;

	}

}
