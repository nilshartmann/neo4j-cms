package nh.cmsvis.importer.actors;

import java.text.SimpleDateFormat;
import java.util.Date;

import nh.cmsvis.importer.Utils;
import nh.cmsvis.importer.model.Branches;
import nh.cmsvis.importer.model.Commit;
import nh.cmsvis.importer.model.Event;
import nh.cmsvis.importer.model.EventCause;
import nh.cmsvis.importer.model.EventState;

public class ReleaseActor extends BranchRelatedActor {

	private int _count = 0;

	public ReleaseActor(Branches branch) {
		super(branch);

	}

	@Override
	protected Commit doAct(Commit newCommit) {

		if (Utils.get().randomIntBetween(1, 9) > 3) {

			System.out.println("RELEASE FOR " + newCommit.getNumber() + "("
					+ newCommit.getMessage() + ")");

			++_count;

			Event.save("R_" + _count, EventCause.release, newCommit,
					Utils.get().randomBoolean() ? EventState.ok
							: EventState.problem, _count + ". Release am "
							+ currentDate());

		}
		return newCommit;
	}

	private String currentDate() {
		SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		return f.format(new Date(Utils.get().currentTime()));
	}
}
