package nh.cmsvis.importer.actors;

import nh.cmsvis.importer.Actor;
import nh.cmsvis.importer.ActorState;
import nh.cmsvis.importer.Szenario;
import nh.cmsvis.importer.model.Branch;
import nh.cmsvis.importer.model.Branches;
import nh.cmsvis.importer.model.Commit;

public abstract class BranchRelatedActor implements Actor {

	private final Branches _branch;

	private Commit _lastCommit = null;

	protected BranchRelatedActor(Branches branch) {
		this._branch = branch;

	}

	@Override
	public ActorState act() throws Exception {
		Branch branch = Szenario.get().getBranch(_branch);

		Commit head = branch.getHead();

		if (!head.equals(_lastCommit)) {
			_lastCommit = doAct(head);
		}

		return ActorState.run;
	}

	protected abstract Commit doAct(Commit newCommit);

	public Branches getBranch() {
		return _branch;
	}

}
