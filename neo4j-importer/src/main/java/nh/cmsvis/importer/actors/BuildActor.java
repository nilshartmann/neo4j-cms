package nh.cmsvis.importer.actors;

import nh.cmsvis.importer.Utils;
import nh.cmsvis.importer.model.Branches;
import nh.cmsvis.importer.model.Build;
import nh.cmsvis.importer.model.Commit;
import nh.cmsvis.importer.model.EventState;

public class BuildActor extends BranchRelatedActor {

	public BuildActor(Branches branch) {
		super(branch);
	}

	@Override
	protected Commit doAct(Commit head) {
		if (Utils.get().randomIntBetween(1, 6) % 2 == 0) {

			Build build = new Build(head, getBranch(), Utils.get()
					.randomBoolean() ? EventState.ok : EventState.problem);

			System.out.println("BUILD FOR BRANCH " + getBranch().name()
					+ " OBJECTID: " + build.getObjectId() + " COMMIT: "
					+ head.getNumber());
			build.save();

		}
		return head;
	}

}
