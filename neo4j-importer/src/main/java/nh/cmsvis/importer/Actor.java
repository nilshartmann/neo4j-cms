package nh.cmsvis.importer;

public interface Actor {

	public ActorState act() throws Exception;

}
