package nh.cmsvis.importer;

import java.util.Random;

public class Time {

	private static final Random r = new Random();
	private static final long MINUTE = 60 * 1000;
	private static final long FIVE_MINUTES = 5 * MINUTE;
	private long current = System.currentTimeMillis();

	public long next() {
		current += (r.nextInt(60) + MINUTE) + FIVE_MINUTES;
		return current;
	}

	public long current() {
		return current;
	}
}
