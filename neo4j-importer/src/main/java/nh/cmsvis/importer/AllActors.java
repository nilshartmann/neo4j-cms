package nh.cmsvis.importer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AllActors {

	private final static AllActors INSTANCE = new AllActors();

	public final static AllActors get() {
		return INSTANCE;
	}

	private final List<Actor> _actors = new ArrayList<>(1000);
	private final List<Actor> _newActors = new LinkedList<>();

	private final List<Actor> _finishingActors = new LinkedList<>();

	public void mergeNewActors() {
		_actors.addAll(_newActors);
		_newActors.clear();
	}

	public List<Actor> getActors() {
		return _actors;
	}

	public void addActor(Actor a) {
		_newActors.add(a);
	}

	public void addFinishingActor(Actor actor) {
		this._finishingActors.add(actor);
	}

	public List<Actor> getFinishingActors() {
		return this._finishingActors;
	}

}
