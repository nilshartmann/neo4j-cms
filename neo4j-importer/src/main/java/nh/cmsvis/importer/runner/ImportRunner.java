package nh.cmsvis.importer.runner;

import java.util.Iterator;
import java.util.List;

import nh.cmsvis.importer.Actor;
import nh.cmsvis.importer.ActorState;
import nh.cmsvis.importer.AllActors;
import nh.cmsvis.importer.Database;
import nh.cmsvis.importer.Szenario;
import nh.cmsvis.importer.actors.BuildActor;
import nh.cmsvis.importer.actors.InstallationActor;
import nh.cmsvis.importer.actors.MergeActor;
import nh.cmsvis.importer.actors.NewChangeActor;
import nh.cmsvis.importer.actors.PullrequestActor;
import nh.cmsvis.importer.actors.ReleaseActor;
import nh.cmsvis.importer.actors.SetHeadActor;
import nh.cmsvis.importer.model.Branches;
import nh.cmsvis.importer.model.EventCause;

public class ImportRunner {

	private int loop;

	public void run() throws Exception {

		final AllActors allActors = AllActors.get();
		boolean stopped = false;

		while (!stopped) {
			loop++;
			System.out.println("Durchgang: " + loop);
			allActors.mergeNewActors();
			Iterator<Actor> actorIterator = allActors.getActors().iterator();

			while (actorIterator.hasNext()) {
				Actor actor = actorIterator.next();

				ActorState state = actor.act();
				if (state == ActorState.finished) {
					actorIterator.remove();
				} else if (state == ActorState.stopAll) {
					stopped = true;
					break;
				}
			}

		}

		System.out.println("RUNNUNG FINISHING ACTORS");
		List<Actor> finishingActors = AllActors.get().getFinishingActors();

		for (Actor actor : finishingActors) {
			actor.act();
		}

	}

	private final static String DB_DIR = "/Users/nils/develop/neo4j/neo4j-community-2.0.3/data/graph.db";

	private final static int CHANGES = 50000;

	public static void main(String[] args) throws Exception {

		// Datenbank erzeugen
		Database.create(DB_DIR);

		// Szenario
		Szenario.setupSzenario();

		// Initiale Aktoren erzeugen
		AllActors allActors = AllActors.get();
		allActors.addActor(new NewChangeActor(CHANGES));
		allActors.addActor(new PullrequestActor());
		allActors.addActor(new BuildActor(Branches.master));
		allActors.addActor(new BuildActor(Branches.integration));
		allActors.addActor(new BuildActor(Branches.bugfix));

		allActors.addActor(new InstallationActor(Branches.bugfix,
				EventCause.DT01));
		allActors.addActor(new InstallationActor(Branches.integration,
				EventCause.DT02));
		allActors.addActor(new InstallationActor(Branches.master,
				EventCause.DT03));

		allActors
				.addActor(new MergeActor(Branches.bugfix, Branches.integration));
		allActors
				.addActor(new MergeActor(Branches.integration, Branches.master));

		allActors.addActor(new ReleaseActor(Branches.bugfix));

		// "Finishing Actors": werden nach dem eigentlich Ablauf EINMALIG
		// ausgeführt
		// allActors.addFinishingActor(new MergeActor(Branches.bugfix,
		// Branches.integration));
		// allActors.addFinishingActor(new MergeActor(Branches.integration,
		// Branches.master));

		allActors.addFinishingActor(new SetHeadActor(Szenario.get()
				.getDevelopmentBranches()));

		// Importer erzeugen
		ImportRunner importRunner = new ImportRunner();

		// Import starten
		importRunner.run();

		Database.shutdown();

	}
}
