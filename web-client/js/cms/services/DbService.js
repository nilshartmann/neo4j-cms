/*******************************************************************************
 * Copyright (c) 2014 Nils Hartmann (http://nilshartmann.net).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nils Hartmann (nils@nilshartmann.net) - initial API and implementation
 ******************************************************************************/

angular.module('cms.services.DbServiceModule', [])
	.constant('CYPHER_URL', 'http://localhost:7474/db/data/cypher')
	.factory('DbService', function ($http, CYPHER_URL) {

		/**
		 * Führt den übergebenen Query aus und ruft das übergebene Callback bzw Model mit der ersten Zeile des Ergebnisses aus
		 * @param query Der Query
		 * @param successCallbackOrModel Das Success-Callback ODER ein Model-Objekt
		 * @param modelProperty Das Property im Model-Objekt, an das das Ergebnis geschrieben wird (nur wenn successCallbackOrModel eine Funktion ist)
		 */
		function runSingleRowQuery(query, successCallbackOrModel, modelProperty) {
			$http.post(CYPHER_URL, {'query': query}).success(function (response) {

				var row = response.data[0];

				if (angular.isFunction(successCallbackOrModel)) {
					successCallbackOrModel(row);
					return;
				}
				successCallbackOrModel[modelProperty] = row[0];
			});
		}

		return {
			loadDatabaseInfo: function (model) {

				runSingleRowQuery('MATCH (c:Commit) RETURN count(c)', model.database, 'commitCount');
				runSingleRowQuery('MATCH (c:Change) RETURN count(c)', model.database, 'changeCount');
				runSingleRowQuery('MATCH (c:Build) RETURN count(c)', model.database, 'buildCount');
				runSingleRowQuery('MATCH (c:Event) WHERE c.cause="pullrequest_merged" RETURN count(c)', model.database, 'pullrequestCount');
				runSingleRowQuery('MATCH (c:Commit) RETURN count(c)', model.database, 'commitCount');
				runSingleRowQuery('MATCH (event:Event) RETURN event.time, event.cause, event.objectId ORDER BY event.time DESC LIMIT 1', function (row) {
					model.database.lastEvent = {
						time:     row[0],
						cause:    row[1],
						objectId: row[2]
					}
				});
			}
		}

	});
