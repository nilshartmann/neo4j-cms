angular.module('cms.views.changelog.ChangelogModule', ['cms.services.DbServiceModule'])
	.controller('ChangelogViewController', function ($scope, $http, $location, DbService, CYPHER_URL) {

		var model = {
			database: {},
			fromTag:  'BUILD_bugfix_1',
			toTag:    'BUILD_bugfix_8'
		};

		function loadChangelogFor(from, to) {
			// MATCH (n)-[:COMMIT]->(commit:Commit) WHERE n.objectId = {from} OR n.objectId = {to} RETURN commit.objectId ORDER BY n.time DESC;
			console.log("CHANGELOG FROM '" + from + "' to '" + to + "'");
			delete model.commits;
			delete model.range;
			model.loading = true;
			// Schritt 1: Aufloesen der Commits
			$http.post(CYPHER_URL,
				{'query':   'MATCH (n)-[:COMMIT]->(commit:Commit) WHERE n.objectId = {from} OR n.objectId = {to} RETURN commit ORDER BY n.time DESC',
					'params': {
						'from': from,
						'to':   to
					}
				}).success(function (response) {
					console.log("ANTWORT:");
					console.dir(response.data);
					if (response.data.length < 2) {
						model.error = "Konnte Referenzen nicht auflösen";
						delete model.loading;
						return;
					}
					var newestCommit = response.data[0][0].data;
					var prevCommit = response.data[1][0].data;

					console.log("PREV COMMIT");
					console.dir(prevCommit);

					var query =
						"MATCH p = (newCommit:Commit  {objectId: '" + newestCommit.objectId + "'})-[:GIT_FIRST_PARENT*0..]->(prevCommit:Commit {objectId: '" + prevCommit.objectId + "'}) RETURN nodes(p)";

					model.range = {
						prevCommit: prevCommit,
						toCommit:   newestCommit,
						query:      query
					};
					model.commits = [];

					// Schritt 2: Laden der Commits
					$http.post(CYPHER_URL,
						{'query': query }
					).success(function (commitResponse) {
							delete model.loading;
							console.log("COMMITS:");
							if (!commitResponse.data || commitResponse.data.length < 1) {
								model.error = "Keine Commits zwischen den Referenzen gefunden";
								delete model.loading;
								return;
							}

							var commits = commitResponse.data[0][0];
							commits.forEach(function (c) {
								var commit = {
									time:    c.data.time,
									sha:     c.data.sha,
									message: c.data.message
								};
								model.commits.push(commit);
							});
							console.dir(commits);
						}).error(function (status) {
							delete model.loading;
							console.log("ERROR BEIM LADEN DER COMMIT-LISTE:");
							console.log(status);

						});
				}).error(function (response) {
					delete model.loading;
					console.log("ERROR!");
				});
		}

		$scope.submit = function () {
			console.log("SUBMIT!");
			delete model.error;

//			$location.path('/' + $scope.model.changeId);
			if (!$scope.model.fromTag) {
				model.error = "Von-Angabe fehlt!";
				return;
			}
			if (!$scope.model.toTag) {
				model.error = "Nach-Angabe fehlt!";
				return;
			}

			loadChangelogFor($scope.model.fromTag, $scope.model.toTag);
		};

		$scope.model = model;
		/*
		 var path = $location.path();
		 if (path && path.length > 1) {
		 path = path.substr(1);
		 model.changeId = path;
		 loadHistoryFor(path);
		 }
		 */

		DbService.loadDatabaseInfo(model);

	})
;