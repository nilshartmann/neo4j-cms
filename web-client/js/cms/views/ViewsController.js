/*******************************************************************************
 * Copyright (c) 2014 Nils Hartmann (http://nilshartmann.net).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nils Hartmann (nils@nilshartmann.net) - initial API and implementation
 ******************************************************************************/

angular.module('cms.views.ViewsModule', []).controller('ViewsController', function ($scope, $location) {

	// TODO: Mit Route-Konfiguration in CmsApp verbinden
	var NAVBAR_ENTRIES = [
		{path: 'changelog', title: 'Changelog'},
		{path: 'search', title: 'Suchen'}
	];

	$scope.$on("$routeChangeSuccess", function () {
		setupNavBar();
	});

	setupNavBar();

	function setupNavBar() {

		var currentPath = $location.path();
		var navBarEntries = [];
		NAVBAR_ENTRIES.forEach(function (n) {
			var entry = angular.copy(n);

			entry.active = currentPath.substr(0, entry.path.length + 1) === '/' + entry.path;

			navBarEntries.push(entry);
		});

		console.log("NAV BAR:");
		console.dir(navBarEntries);

		$scope.navBar = {
			entries: navBarEntries
		};

	}
});

