/*******************************************************************************
 * Copyright (c) 2014 Nils Hartmann (http://nilshartmann.net).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nils Hartmann (nils@nilshartmann.net) - initial API and implementation
 ******************************************************************************/
angular.module('cms.views.search.SearchModule').directive('cmsSearchResultRow', function () {
	console.log("DIRECTIVE!!!");
	return {
		restrict:    'A',
		scope:       {
			title: '=title',
			entry: '=entry'

		},
		templateUrl: 'js/cms/views/search/search-result-row-template.html'
	}
});