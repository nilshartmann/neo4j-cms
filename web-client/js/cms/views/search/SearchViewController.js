angular.module('cms.views.search.SearchModule')
	.controller('SearchViewController', function ($scope, $http, $location, $routeParams, DbService, CYPHER_URL, Utils) {

		var model = {
			database: {
				queryTimeout: 10
			}
		};

		// === BRANCH-UNABHAENGIG ======
		function loadPullrequest(history) {
			var q = "MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit)<-[:COMMIT]-(event:Event) WHERE change.changeId = {changeId} AND event.cause='pullrequest_merged' RETURN event LIMIT 1;"
			runCypherQueryForHistoryChange(history, 'pullrequest', q);
		}

		// === ZIEL-BRANCH:BUGFIX ======
		function loadGesamtBuildBugfix(history) {
			runCypherQueryForHistoryChange(history, 'gesamtBuildBugfix', 'MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit) <-[:PREV_COMMIT*0..]-(commit:Commit)<-[:COMMIT]-(build:Build) WHERE change.objectId = {changeId} RETURN build LIMIT 1;');
		}

		function loadDT01(history) {
			var q = 'MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit) <-[:PREV_COMMIT*0..]-(commit:Commit)<-[:COMMIT]-(event:Event)	WHERE change.objectId = {changeId} AND event.cause = "DT01" RETURN event LIMIT 1;';
			runCypherQueryForHistoryChange(history, 'dt01', q);
		}

		function loadMergeBugfixInt(history) {
			var q = "MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..]->(commit:Commit)-[:MERGE_DOWN]->(mergeCommit:Commit)"
				+ " WHERE change.objectId = {changeId} RETURN mergeCommit LIMIT 1";

			runCypherQueryForHistoryChange(history, 'mergeBugfixInt', q);
		}

		function loadGesamtBuildIntViaBugfix(history) {
			var q = "MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..]->(commit:Commit)-[:MERGE_DOWN]->(mergeCommit:Commit)"
				+ " WHERE change.objectId = {changeId} WITH changeCommit,mergeCommit LIMIT 1"
				+ " MATCH (mergeCommit)-[:FIRST_NEXT_COMMIT*0..1000]->(buildCommit:Commit)<-[:COMMIT]-(build:Build) WHERE build.time >= changeCommit.time"
				+ " WITH build,mergeCommit LIMIT 1"
				+ " RETURN build";

			runCypherQueryForHistoryChange(history, 'gesamtBuildInt', q);
		}

		function loadDT02ViaBugfix(history) {
			var q = "MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..]->(commit:Commit)-[:MERGE_DOWN]->(mergeCommit:Commit)"
				+ " WHERE change.objectId = {changeId} WITH changeCommit,mergeCommit LIMIT 1"
				+ " MATCH (mergeCommit)-[:FIRST_NEXT_COMMIT*0..1000]->(eventCommit:Commit)-[:EVENT]->(event:Event) WHERE event.cause = 'DT02' AND event.time >= changeCommit.time"
				+ " WITH event,mergeCommit LIMIT 1"
				+ " RETURN event,mergeCommit;";
			runCypherQueryForHistoryChange(history, 'dt02', q);
		}

		function loadMergeIntMasterViaBugfix(history) {
			var q = "MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..1000]->(commit:Commit)-[:MERGE_DOWN]->(intMergeCommit:Commit)"
				+ " WHERE change.objectId = {changeId}"
				+ " WITH changeCommit,intMergeCommit LIMIT 1"
				+ " MATCH (intMergeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..1000]->(:Commit)-[:MERGE_DOWN]->(masterMergeCommit:Commit)"
				+ " RETURN masterMergeCommit LIMIT 1 ";

			runCypherQueryForHistoryChange(history, 'mergeIntMaster', q);
		}

		function loadGesamtBuildMasterViaBugfix(history) {
			var q = "MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..1000]->(commit:Commit)-[:MERGE_DOWN]->(intMergeCommit:Commit)"
				+ " WHERE change.objectId = {changeId}"
				+ " WITH changeCommit,intMergeCommit LIMIT 1"
				+ " MATCH (intMergeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..1000]->(:Commit)-[:MERGE_DOWN]->(masterMergeCommit:Commit)"
				+ " WITH changeCommit,masterMergeCommit LIMIT 1"
				+ " MATCH (masterMergeCommit)-[:FIRST_NEXT_COMMIT*0..1000]->(dt03Commit:Commit)<-[:COMMIT]-(dt03Build:Build)"
				+ " WHERE dt03Build.time>=changeCommit.time"
				+ " RETURN dt03Build LIMIT 1;";

			runCypherQueryForHistoryChange(history, 'gesamtBuildMaster', q);
		}

		function loadDT03ViaBugfix(history) {
			var q = "MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..1000]->(commit:Commit)-[:MERGE_DOWN]->(intMergeCommit:Commit)"
				+ " WHERE change.objectId = {changeId}"
				+ " WITH changeCommit,intMergeCommit LIMIT 1"
				+ " MATCH (intMergeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..1000]->(:Commit)-[:MERGE_DOWN]->(masterMergeCommit:Commit)"
				+ " WITH changeCommit,masterMergeCommit LIMIT 1"
				+ " MATCH (masterMergeCommit)-[:FIRST_NEXT_COMMIT*0..1000]->(dt03Commit:Commit)-[:EVENT]->(dt03Event:Event)"
				+ " WHERE dt03Event.cause = 'DT03' AND dt03Event.time>=changeCommit.time"
				+ " RETURN dt03Event LIMIT 1;";

			runCypherQueryForHistoryChange(history, 'dt03', q);
		}

		// ZIEL-BRANCH: integration
		function loadGesamtBuildInt(history) {
			// query identich mit loadGesamtBuildBugfix
			runCypherQueryForHistoryChange(history, 'gesamtBuildInt', 'MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit) <-[:PREV_COMMIT*0..]-(commit:Commit)<-[:COMMIT]-(build:Build) WHERE change.objectId = {changeId} RETURN build LIMIT 1;');
		}

		function loadDT02ViaInt(history) {
			var q = 'MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit) <-[:PREV_COMMIT*0..]-(commit:Commit)<-[:COMMIT]-(event:Event)	WHERE change.objectId = {changeId} AND event.cause = "DT02" RETURN event LIMIT 1;';
			runCypherQueryForHistoryChange(history, 'dt02', q);
		}

		function loadMergeIntMaster(history) {
			var q = "MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..]->(commit:Commit)-[:MERGE_DOWN]->(mergeCommit:Commit)"
				+ " WHERE change.objectId = {changeId} RETURN mergeCommit LIMIT 1";

			runCypherQueryForHistoryChange(history, 'mergeIntMaster', q);
		}

		function loadGesamtBuildMasterViaInt(history) {
			var q = "MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..]->(commit:Commit)-[:MERGE_DOWN]->(mergeCommit:Commit)"
				+ " WHERE change.objectId = {changeId} WITH changeCommit,mergeCommit LIMIT 1"
				+ " MATCH (mergeCommit)-[:FIRST_NEXT_COMMIT*0..1000]->(buildCommit:Commit)<-[:COMMIT]-(build:Build) WHERE build.time >= changeCommit.time"
				+ " WITH build,mergeCommit LIMIT 1"
				+ " RETURN build";

			runCypherQueryForHistoryChange(history, 'gesamtBuildMaster', q);
		}

		function loadDT03ViaInt(history) {
			var q = "MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit)-[:FIRST_NEXT_COMMIT*0..]->(commit:Commit)-[:MERGE_DOWN]->(mergeCommit:Commit)"
				+ " WHERE change.objectId = {changeId} WITH changeCommit,mergeCommit LIMIT 1"
				+ " MATCH (mergeCommit)-[:FIRST_NEXT_COMMIT*0..1000]->(eventCommit:Commit)-[:EVENT]->(event:Event) WHERE event.cause = 'DT03' AND event.time >= changeCommit.time"
				+ " WITH event,mergeCommit LIMIT 1"
				+ " RETURN event,mergeCommit;";
			runCypherQueryForHistoryChange(history, 'dt03', q);
		}

		// ZIEL-BRANCH: master ===============================================================================
		function loadGesamtBuildMaster(history) {
			// query identich mit loadGesamtBuildBugfix
			runCypherQueryForHistoryChange(history, 'gesamtBuildMaster', 'MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit) <-[:PREV_COMMIT*0..]-(commit:Commit)<-[:COMMIT]-(build:Build) WHERE change.objectId = {changeId} RETURN build LIMIT 1;');
		}

		function loadDT03(history) {
			var q = 'MATCH (change:Change)-[:COMMIT]->(changeCommit:Commit) <-[:PREV_COMMIT*0..]-(commit:Commit)<-[:COMMIT]-(event:Event)	WHERE change.objectId = {changeId} AND event.cause = "DT03" RETURN event LIMIT 1;';
			runCypherQueryForHistoryChange(history, 'dt03', q);
		}

		function runCypherQueryForHistoryChange(history, historyEntry, query) {
			var data = {
				'query':  query,
				'params': {
					'changeId': history.change.changeId
				}
			};

			runCypherQuery(history, historyEntry, data);
		}

		function runCypherQuery(history, historyEntry, query) {
			history[historyEntry] = {
				loading: true,
				query:   query.query.replace('{changeId}', '"' + query.params.changeId + '"')
			};

			var start = new Date().getTime();

			// : 500
			var t = model.database.queryTimeout * 1000;
			console.log("T: " + t + ", type: " + typeof t);
			$http.post(CYPHER_URL, query, {headers: {'max-execution-time': t}}).success(function (response) {
				var executionTime = Utils.msToTime(new Date().getTime() - start);
				if (response.data.length < 1) {
					history[historyEntry] = {
						'state':         'pending',
						'description':   'Ausstehend',
						'executionTime': executionTime
					};
					return;
				}
				var event = response.data[0][0].data;
				console.log("STATE for " + historyEntry + ": " + event.state);
				history[historyEntry] = {
					'time':          event.time,
					'objectId':      event.objectId,
					'description':   event.summary,
					'state':         event.state || 'ok',
					'executionTime': executionTime
				};

			}).error(function (data, status) {
				var executionTime = Utils.msToTime(new Date().getTime() - start);
				console.log("ERROR " + status + " for " + historyEntry + ":");
				console.dir(data);

				history[historyEntry] = {
					'state':         'problem',
					'objectId':      'ERROR ' + status,
					'description':   data.message,
					'executionTime': executionTime,
					query:           query.query.replace('{changeId}', '"' + query.params.changeId + '"')
				};
			});

		}

		function loadHistoryFor(changeId) {
			console.log("LOAD HISTORY FOR '" + changeId + "'");
			model.history = {};
			model.error = '';

			//
			var data = {
				'query':  'MATCH (change:Change) WHERE change.changeId={changeId} RETURN change;',
				'params': {
					'changeId': changeId
				}
			};
			$http.post(CYPHER_URL, data).success(function (response) {
				if (response.data.length < 1) {
					// Change nicht vorhanden
					model.error = 'Change nicht bekannt';
					return;
				}

				model.history.change = response.data[0][0].data;
				console.log("model.history.change:");
				console.dir(model.history.change);

				loadPullrequest(model.history);

				if (model.history.change.targetBranch === 'bugfix') {
					loadGesamtBuildBugfix(model.history);
					loadDT01(model.history);
					loadMergeBugfixInt(model.history);

					loadGesamtBuildIntViaBugfix(model.history);
					loadDT02ViaBugfix(model.history);
					loadMergeIntMasterViaBugfix(model.history);
					loadGesamtBuildMasterViaBugfix(model.history);
					loadDT03ViaBugfix(model.history);

				} else if (model.history.change.targetBranch === 'integration') {
					console.log("INTEGRATION BRANCH");
					loadGesamtBuildInt(model.history);
					loadDT02ViaInt(model.history);

					loadMergeIntMaster(model.history);
					loadGesamtBuildMasterViaInt(model.history);
					loadDT03ViaInt(model.history);
				} else if (model.history.change.targetBranch === 'master') {
					loadGesamtBuildMaster(model.history);
					loadDT03(model.history);
				}
			}).error(function (data, status) {
				console.log("ERROR!!!: " + status);
			});
		}

		function setLocationFromChangeId() {
			var p = 'search/' + $scope.model.changeId;
			$location.path(p);
		}

		$scope.reset = function () {
			model.history = {};
			model.error = '';
			model.changeId = '';
			setLocationFromChangeId();
		};

		$scope.submit = function () {
			setLocationFromChangeId();

			loadHistoryFor($scope.model.changeId);
		};

		$scope.model = model;

		var path = $routeParams.changeId;
		console.log("PATH FROM ROUTE PARAMS: " + path);
		if (path && path.length > 0) {
			model.changeId = path;
			loadHistoryFor(path);
		}

		DbService.loadDatabaseInfo(model);
	})

;