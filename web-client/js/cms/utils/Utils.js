/*******************************************************************************
 * Copyright (c) 2014 Nils Hartmann (http://nilshartmann.net).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nils Hartmann (nils@nilshartmann.net) - initial API and implementation
 ******************************************************************************/
angular.module('cms.utils.UtilsModule')
	.factory('Utils', function () {
		function msToTime(duration) {

			if (duration < 1000) {
				return duration + "ms";
			}

			var milliseconds = parseInt((duration % 1000) / 100)
				, seconds = parseInt((duration / 1000) % 60)
				, minutes = parseInt((duration / (1000 * 60)) % 60);

			minutes = (minutes < 10) ? "0" + minutes : minutes;
			seconds = (seconds < 10) ? "0" + seconds : seconds;

			var result = "";
			var unit = 's';
			if (minutes !== '00') {
				result = result + minutes + ":";
				unit = 'm'
			}

			return result + seconds + "." + milliseconds + unit;
		}

		return {
			msToTime: msToTime
		}

	});