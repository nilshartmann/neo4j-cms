angular.module('cms.utils.UtilsModule')
	.directive('capitalize', function () {
		// http://stackoverflow.com/a/16388643
		return {
			require: 'ngModel',
			link:    function (scope, element, attrs, modelCtrl) {
				var capitalize = function (inputValue) {
					var capitalized = inputValue;
					if (capitalized) {
						capitalized = inputValue.toUpperCase();
						capitalized = capitalized.replace('_', '-');
					}
					if (capitalized !== inputValue) {
						modelCtrl.$setViewValue(capitalized);
						modelCtrl.$render();
					}
					return capitalized;
				};
				modelCtrl.$parsers.push(capitalize);
				capitalize(scope[attrs.ngModel]);  // capitalize initial value
			}
		};
	});