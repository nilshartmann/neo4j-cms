/*******************************************************************************
 * Copyright (c) 2014 Nils Hartmann (http://nilshartmann.net).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nils Hartmann (nils@nilshartmann.net) - initial API and implementation
 ******************************************************************************/

/*


 'cms.views.changelog.ChangelogModule',


 */
angular.module('cms.CmsApp', [
	'ngRoute',
	'cms.utils.UtilsModule',
	'cms.views.ViewsModule',
	'cms.views.search.SearchModule',
	'cms.views.changelog.ChangelogModule'
])

	.config(function ($routeProvider) {

		console.log("cms.CmsApp");

		$routeProvider
			.when('/search/:changeId?', {templateUrl: 'js/cms/views/search/search-view.html'})
			.when('/changelog', {templateUrl: 'js/cms/views/changelog/changelog-view.html'})
			.otherwise({redirectTo: 'search'})

	});
